#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
VERSION=$2
DIR=json-lib-$VERSION
TAR=../libjson-java_$VERSION.orig.tar.xz

# clean up the upstream tarball
mkdir $DIR
(cd $DIR && jar xf ../$3)
XZ_OPT=--best tar -c -J -v -f $TAR $DIR
rm -rf $DIR $3
